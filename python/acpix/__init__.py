#!/home/donal/Geek/acpix/venv/bin/pypy -u


from twisted.internet.protocol import Factory
from twisted.internet import (
    endpoints,
    reactor,
    defer,
    protocol
)
from twisted.protocols import basic



class AcpiLineReceiver(basic.LineReceiver):
    def dataReceived(self, data):
        print("DATA RECEIVED: {}".format(data))
        return basic.LineReceiver.dataReceived(data)

    def lineReceived(self, line):
        print("DATA RECEIVED: {}".format(line))
        return basic.LineReceiver.lineReceived(line)


def doke(ok):
    print("ok: {}".format(ok))

def main_async():
    socket_path = "/var/run/acpid.socket"
    ep = endpoints.UNIXClientEndpoint(reactor, socket_path)
    factory = protocol.Factory.forProtocol(AcpiLineReceiver)
    df = ep.connect(factory)
    df.addCallback(doke)
    return df


def main():
    reactor.callWhenRunning(main_async)
    reactor.run()

main()
